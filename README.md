# Heating technologies data for EU28 

In this repository, heating technologies for each member state are describe and differentiated by country, year, type of equipment, type of building, 
price, labor, and equipment maintenance indices, specific investment costs, equipment cost share, installation cost share, fixed and variable O&M, technical lifetime, and total annual efficiency.


## Repository structure

Files:
```
datapackage.json                         -- Datapackage JSON file with the main meta-data
data/Heating_technologies_EU28_v3.csv    -- Tabular heating technologies and indicators

```

## Documentation

In order to divide the H&C technologies in different capacity groups, four type of buildings are analysed: 

•	Existing SFH with c.a. 10-15 kWth installed capacity  
•	Existing MFH with max 400 kWth installed capacity  
•	New SFH with c.a. 4-10 kWth installed capacity  
•	New MFH with max 160 kWth installed capacity  

As a starting and reference point, technology data for individual heating plants from the Danish Energy Agency (DEA) catalogue were used [1].
In order to define the costs for each MS based on the data from the catalogue, labour, material, and maintenance costs indices are determined with Denmark as a referent MS.
By multiplying these indices with the cost data from the DEA, country specific values are calculated. 
For the labour cost index data from EUROSTAT for the cost per hour in the construction sector in 2016 was used [2], whereas for the price index the home appliances index from the price level indices group was used [3]. 
As for the equipment and maintenance, household furnishings, equipment and maintenance index [4] was used.

For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps WP2 report](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) section 2.5.2 page 97ff.

### Limitations of the datasets

While the generated technical and financial data are providing sufficient and reliable information's about the individual H&C technologies currently used in the existing EU building stock, forecasting the future market developments can pose a major challenge. 
Even though lower investment prices are predicted and expected for some technologies like HPs and solar thermal collector, the forecasted price development may vary from one country to another.
Furthermore, the price and labour cost indices presented in this study are constant over the years in order to simplify the calculation process even though this certainly will not be the real case and similarly to the forecasted price developments, should be taken with a dose of uncertainty.
In order to improve the quality and reliability of the data, future updates are necessary. 

### References

[1]	Danish energy agency (DEA), “Individual Heating Plants and Energy Transport, Technology data for energy heating installations.”  
[2]	Eurostat, “Labour cost levels by NACE Rev. 2 activity [Call ID: lc_lci_lev],” 08-Jun-2017.  
[3]	Eurostat, “Price index for household appliances based upon purchasing power parities (PPPs), price level indices and real expenditures for ESA 2010 aggregates [Call ID: prc_ppp_ind],” 14-Dec-2017  
[4]	Eurostat, “Household furnishings, equipment and maintenance index based upon purchasing power parities (PPPs), price level indices and real expenditures for ESA 2010 aggregates [Call ID: prc_ppp_ind].”  

## How to cite

Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) 

## Authors
Eftim Popovski <sup>*</sup>,

<sup>*</sup> [Fraunhofer ISI](https://www.isi.fraunhofer.de/en.html)  
Fraunhofer Institute for Systems and Innovation Research  
Breslauer Strasse 48  
76139 Karlsruhe  


## License
Copyright © 2016 - 2020, Eftim Popovski

SPDX-License-Identifier: CDLA-Permissive-1.0

License-Text: [Community Data License Agreement Permissive 1.0](https://cdla.io/permissive-1-0/)

This work is licensed under a Community Data License Agreement – Permissive – Version 1.0


## Acknowledgement
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.